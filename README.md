Centos7 with ownconda
=====================

This is a minimal Centos7 image with our ownconda Python distribution.


## Run the container

```bash
$ docker run --rm -it -v ~/Projects:/projects forge.services.own/centos7-ownconda-runtime bash
```


## Build an run the container locally

You can build and run the container locally:

```bash
$ docker build --build-arg dist_url='https://forge.services.own/conda/stable/ownconda.sh' -t centos7-ownconda-runtime .
$ docker run --rm -it -v ~/Projects:/projects centos7-ownconda-runtime bash
```

See `docker build --help` or `docker run --help` for details.
